# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Dependencies
* Setup and run
* ERD
* Apis spec
* Rake tasks
* Workers

### Quick summary ###
- This project is mini Ruby web api services without using Rails or any other ruby framework.
- Using Puma web server with Rack gem.
- Using `activerecord` as ORM.
- Using `actionpack` for routing and controllers.
- Using Active support core extensions.
- Following RESTFUL api concept.
- Using MVC pattern.
- The web server configs in `config.ru`
- The Basic application configs, setup and routing in `application.rb`
- In `controllers` folder listing all the controllers files
- In `models` folder listing all the entities definition
- In `serializers` folder listing all json serializering models
- In `lib/tasks` folder listing all the rake tasks

### Dependceies ###
- Ruby version `2.5.9`
- DataBase Sqlite3

### Setup and run ###
- To install all dependencies gems
```
bundle install
```

- To create and migrate the DB schema
```
rake db:migrate
```

- To create data
```
rake db:seeds
```

- To run the Puma server using Rack
```bash
rackup config.ru
```

### ERD ###
[ERD Diagram](https://dbdiagram.io/d/61a5ee958c901501c0d8fe0c)

![ERD Diagram](https://bitbucket.org/repo/e5zpdBX/images/2877833108-posts_api_service.png)

### Rake tasks ###
- To list all avaialble rake tasks:
```bash
> rake --tasks
rake db:migrate                           # migrate
rake db:seeds                             # seeds
rake feedbacks:generate_xml[folder_path]  # generate_xml
```

### Workers ###
- Using `whenever` ruby gem to crate cronjob rules
- all the avaialble cronjob is listed in `schedule.rb` file
```ruby
every :day, at: '09:00am' do
  rake "feedbacks:generate_xml[\"$(pwd)/tmp/feedbacks_xml_files\"]"
end
```
- To update the cronjob run
```bash
whenever --update-crontab
```
- Created rake task to generate XML feedbacks files in batches, check `feedbacks.rake`
- And calling this rake task using cronjob every day at 9AM
- The `feedbacks:generate_xml` rake task will fetch all the feedbacks in batches from DB query directly and export it as xml in batches files under folder with the executed date under absolute param folder_path.

### APIs Spec ###

#### User APIs
- User signup
```bash
curl -X POST -H 'Content-Type: application/json' \
-d '{"name":"User name", "username": "uniq_username", "password": "password"}' \
http://localhost:3000/api/v1/signup
```

- User login
```bash
curl -X POST -H 'Content-Type: application/json' \
-d '{"username": "uniq_username", "password": "password"}' \
http://localhost:3000/api/v1/login
```

- Get user profile
```bash
curl -X GET -H 'Content-Type: application/json' \
-H 'Authorization: Token <valid-access-token>' \
http://localhost:3000/api/v1/users/me
```

- User logout
```bash
curl -X DELETE -H 'Content-Type: application/json' \
-H 'Authorization: Token <valid-access-token>' \
http://localhost:3000/api/v1/logout
```

#### Post APIs
- Create post from logged in user
```bash
curl -X POST -H 'Content-Type: application/json' \
-H 'Authorization: Token <valid-access-token>' \
-d '{"title": "title", "content": "content"}' http://localhost:3000/api/v1/posts
```

- Create post with creating new user
```bash
curl -X POST -H 'Content-Type: application/json' \
-d '{"title": "title", "content": "content", "username": "username", "password": "password"}' http://localhost:3000/api/v1/posts
```

- Get list of posts
```bash
curl -X GET -H 'Content-Type: application/json' \
http://localhost:3000/api/v1/posts/
```

- Get a single post
```bash
curl -X GET -H 'Content-Type: application/json' \
http://localhost:3000/api/v1/posts/1
```

#### Rating APIs
- Create post ratings
```bash
curl -X POST -H 'Content-Type: application/json' \
-H 'Authorization: Token <valid-access-token>' \
-d '{"value": 1}' http://localhost:3000/api/v1/posts/1/ratings
```

#### Feedbacks APIs
- Create post feedback
```bash
curl -X POST -H 'Content-Type: application/json' \
-H 'Authorization: Token <valid-access-token>' \
-d '{"comment": "comment text"}' http://localhost:3000/api/v1/posts/1/feedbacks
```
- Create user feedback
```bash
curl -X POST -H 'Content-Type: application/json' \
-H 'Authorization: Token <valid-access-token>' \
-d '{"comment": "comment text"}' http://localhost:3000/api/v1/users/1/feedbacks
```

#### Author IPs APIs
- Get list of author IPs with logins grouped by IP
```bash
curl -X GET -H 'Content-Type: application/json' \
-H 'Authorization: Token <valid-access-token>' \
http://localhost:3000/api/v1/posts/ip_addresses
```
