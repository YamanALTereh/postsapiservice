Dir[File.join(__dir__, 'controllers', '*.rb')].each { |file| require file }
Dir[File.join(__dir__, 'models', '*.rb')].each { |file| require file }
Dir[File.join(__dir__, 'serializers', '*.rb')].each { |file| require file }
require 'byebug'

# Create router to manage endpoints 
APP_ROUTER = ActionDispatch::Routing::RouteSet.new

APP_ROUTER.draw do
  root to: proc { [200, {}, ['App is alive']] }

  get '/heartbeat', to: proc { [200, {}, [{message: 'App is alive'}.to_json]] }

  scope '/api', :defaults => { :format => 'json' } do
    scope '/v1' do
      resources :posts, only: [:index, :create, :show] do
        resources :ratings, only: [:create]
        resources :feedbacks, only: [:create]
        collection do
          get 'ip_addresses' => 'ip_addresses#index'
        end
      end
      resources :users do
        resources :feedbacks, only: [:create]
        collection do
          get 'me' => 'users#show'
        end
      end
      post 'signup' => 'users#create'
      post 'login' => 'user_sessions#create'
      delete 'logout' => 'user_sessions#destroy'
    end
  end
end

# Create a connection between AR and the database
ActiveRecord::Base.establish_connection(
  adapter: "sqlite3",
  database: "database.sqlite3"
)
