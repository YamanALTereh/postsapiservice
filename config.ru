require 'rack'
require 'rack/reloader'
require 'rack/handler/puma'
require 'action_controller'
require 'action_dispatch'
require 'active_record'
require 'active_model_serializers'
require "action_controller/serialization"
require_relative 'application'
# require ::File.expand_path('../config/environment',  __FILE__)

use Rack::Reloader

Rack::Handler::Puma.run(
  APP_ROUTER,
  Port: 3000, Verbose: true
)
