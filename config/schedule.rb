# RACK_ENV=development whenever --update-crontab

every :day, at: '09:00am' do
  rake "feedbacks:generate_xml[\"$(pwd)/tmp/feedbacks_xml_files\"]"
end

# every :minute do
#   rake "feedbacks:generate_xml[\"$(pwd)/tmp/feedbacks_xml_files\"]"
# end
