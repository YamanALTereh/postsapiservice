class ApplicationController < ActionController::API
  include ActionController::Helpers
  include ActionController::Serialization

  before_action :authenticate_user
  helper_method :current_user, :logged_in?

  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  protected 

  def remote_ip(request)
    request.headers['HTTP_X_FORWARDED_FOR']&.split(',')&.first&.strip || request.ip
  end

  def authenticate_user
    unless current_user
      access_denied
    end
  end

  def access_token
    return unless request.headers['Authorization']
    token = request.headers['Authorization'].split(' ')[1]
    UserAccessToken.active.find_by_token(token)
  end

  def current_user
    return unless access_token
    @current_user ||= User.find_by_id(access_token.user_id)
  end

  def authenticate
    logged_in? ? true : access_denied
  end

  def logged_in?
    current_user.present?
  end

  def record_not_found
    render json: {
      error: 'record not found'
    }, status: 404
  end

  def access_denied
    render json: {
      error: 'Access Denied'
    }.to_json, status: 403
  end
end
