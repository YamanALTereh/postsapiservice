class FeedbacksController < ApplicationController
  def create
    feedback = own_feedbacks.create(feedbacks_params)

    if feedback.save
      render json: own_feedbacks
    else
      render json: {
        error: feedback.errors.messages
      }.to_json, :status => 422
    end
  end

  def index
    render json: own_feedbacks
  end

  private

  def own_feedbacks
    current_user.own_feedbacks
  end

  def feedback_resource
    Post.find_by_id(params[:post_id]) ||
      User.find_by_id(params[:user_id])
  end

  def feedbacks_params
    params.permit(
      :comment
    ).to_h.merge(
      resource: feedback_resource,
      owner_id: current_user.id
    )
  end
end
