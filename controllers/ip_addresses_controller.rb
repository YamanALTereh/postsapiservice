class IpAddressesController < ApplicationController
  DEFAULT_PER_LIMIT = 100;

  # TODO, Implement pagination
  def index
    render json: serialized_results
  end

  private

  def serialized_results
    grouped_ip_addresses.map { |e| JSON.parse(e['data']) }
  end

  # TODO, use Arel here instead of raw sql
  # Listed only the Ips that the user submitted one post at least with it
  def grouped_ip_addresses
    sql = "
      SELECT
        json_object(
          'ip_address', elem.ip_address, 'user_id', elem.user_id, 'logins', json_group_array(
            json_object(
              'token', elem.token,
              'created_at', elem.created_at
            )
          )
        ) as data
      FROM user_access_tokens elem
      JOIN posts on posts.user_id = elem.user_id
      GROUP BY elem.ip_address
      ORDER BY elem.created_at desc
      LIMIT #{DEFAULT_PER_LIMIT}
      ;  
    "
    ActiveRecord::Base.connection.execute(sql)
  end
end
