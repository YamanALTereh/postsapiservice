class PostsController < ApplicationController
  skip_before_action :authenticate_user

  DEFAULT_PER_LIMIT = 25

  # TODO, need to implement pagination
  def index
    # TODO, is it Post.all or user.posts?
    posts = Post.all.order(
      'ratings_total / ratings_count desc'
    ).limit(
      DEFAULT_PER_LIMIT
    )
    render json: posts
  end

  def create
    # Create new user if not exist
    unless logged_in?
      unless new_user.save
        access_denied
      end
    end

    post = Post.new(post_params)

    if post.save
      render json: post
    else
      render json: {
        error: post.errors.messages
      }.to_json, :status => 422
    end
  end

  def show
    # post = current_user.posts.find(params[:id])
    post = Post.find(params[:id])
    render json: post
  end

  private

  def post_params
    params.permit(
      :title, :content
    ).to_h.merge(
      user_id: (current_user || new_user.reload).id,
      author_ip: remote_ip(request)
    )
  end

  def new_user
    return nil if current_user.present?
    @new_user ||= User.new(
      user_params
    )
  end

  def user_params
    params.permit(
      :username, :password
    ).to_h.merge(
      last_login: Time.current
    )
  end
end
