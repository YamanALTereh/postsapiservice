class RatingsController < ApplicationController
  def create
    rating = post.ratings.create(ratings_params)

    if rating.save
      render json: post.reload
    else
      render json: {
        error: rating.errors.messages
      }.to_json, :status => 422
    end
  end

  def show
    post = current_user.posts.find(params[:id])
    render json: post
  end

  private

  def post
    @post ||= Post.find(params[:post_id])
  end

  def ratings_params
    params.permit(
      :post_id, :value
    ).to_h.merge(
      user_id: current_user.id
    )
  end
end
