class UserSessionsController < ApplicationController
  skip_before_action :authenticate_user, only: [:create]

  def create
    @user = User.find_by_username(params[:username])

    if @user.authenticate(params[:password])
      created_access_token

      render json: {
        token: created_access_token.token
      }
    else
      access_denied
    end
  end

  def destroy
    if access_token
      access_token.revoke!
      render json: {
        success: true
      }
    else
      access_denied
    end
  end

  private

  def created_access_token
    @created_access_token ||= UserAccessToken.create!(
      user_id: @user.id,
      ip_address: remote_ip(request)
    )
  end
end
