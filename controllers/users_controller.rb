class UsersController < ApplicationController
  skip_before_action :authenticate_user, only: [:create]

  def create
    user = User.new(user_params)

    if user.save
      render json: user
    else
      render json: {
        error: user.errors.messages
      }.to_json, :status => 422
    end
  end

  def show
    render json: current_user
  end

  private

  def user_params
    params.permit(
      :username, :password, :name
    ).to_h.merge(
      last_login: Time.current
    )
  end
end
