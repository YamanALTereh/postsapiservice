require 'faker'
require 'activerecord-import'

namespace :db do
  desc 'migrate'
  task :migrate do
    puts "starting migration ..."
    queries = <<~SQL.squish
      CREATE TABLE IF NOT EXISTS users (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        username TEXT,
        name TEXT,
        last_login TEXT,
        password_digest TEXT,
        created_at TEXT,
        updated_at TEXT
      );
      CREATE UNIQUE INDEX IF NOT EXISTS users_username_idx ON users (username);

      CREATE TABLE IF NOT EXISTS user_access_tokens (
        user_id INTEGER,
        token TEXT PRIMARY KEY,
        created_at TEXT,
        revoked_at TEXT,
        expired_at TEXT,
        ip_address TEXT
      );
      CREATE UNIQUE INDEX IF NOT EXISTS user_access_tokens_token_idx ON user_access_tokens (token);

      CREATE TABLE IF NOT EXISTS posts (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        user_id INTEGER,
        title TEXT,
        content TEXT,
        author_ip TEXT,
        ratings_count INTEGER DEFAULT 0,
        ratings_total INTEGER DEFAULT 0,
        created_at TEXT,
        updated_at TEXT
      );
      CREATE INDEX IF NOT EXISTS posts_user_id_idx ON posts (user_id);

      CREATE TABLE IF NOT EXISTS ratings (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        post_id INTEGER,
        user_id INTEGER,
        value INTEGER,
        created_at TEXT,
        updated_at TEXT
      );
      CREATE INDEX IF NOT EXISTS ratings_user_id_idx ON ratings (user_id);
      CREATE INDEX IF NOT EXISTS ratings_post_id_idx ON ratings (post_id);

      CREATE TABLE IF NOT EXISTS feedbacks (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        resource_type TEXT,
        resource_id INTEGER,
        owner_id INTEGER,
        comment TEXT,
        created_at TEXT,
        updated_at TEXT
      );
      CREATE INDEX IF NOT EXISTS feedbacks_owner_id_idx ON feedbacks (owner_id);
      CREATE INDEX IF NOT EXISTS feedbacks_resource_type_resource_id_id_idx ON feedbacks (resource_type, resource_id);
    SQL

    queries.split(';').each do |query|
      ActiveRecord::Base.connection.execute(query)
    end
    puts "done migration."
  end

  desc 'seeds'
  task :seeds do
    count = 100
    puts "Creating users ..."
    new_records = count.times.map do |_|
      User.new(
        name: Faker::Name.name,
        username: "#{Faker::Internet.unique.username}_#{Time.now.to_i}",
        password: Faker::Internet.password,
        last_login: Time.at(1.month.ago + rand * (Time.current.to_f - 1.month.ago.to_f))
      )
    end
    res = User.import(new_records, on_duplicate_key_ignore: true)

    count = 200
    user_ids = User.all.pluck(:id)

    puts "Creating Access tokens ..."
    new_records = count.times.map do |_|
      UserAccessToken.new(
        user_id: user_ids.sample,
        token: SecureRandom.hex,
        ip_address: Faker::Internet.public_ip_v4_address,
        expired_at: Time.at(1.month.ago + rand * (3.months.from_now.to_f - 1.month.ago.to_f))
      )
    end
    UserAccessToken.import(new_records)

    puts "Creating posts ..."
    new_records = count.times.map do |_|
      ratings_count = rand(0..1000)

      Post.new(
        user_id: user_ids.sample,
        title: Faker::Book.title,
        content: Faker::Books::Lovecraft.paragraph,
        author_ip: Faker::Internet.public_ip_v4_address,
        ratings_total: rand(ratings_count..ratings_count*5),
        ratings_count: ratings_count
      )
    end
    Post.import(new_records)

    # Use Set to avoid any uniq validation error
    def seeds_feedbacks(count = 5, resource_type = 'Post')
      counter = 0
      user_ids = User.all.pluck(:id)
      while counter < count
        resource_type_ids = resource_type.camelize.constantize.all.pluck(:id)
        begin
          asd = Feedback.create!(
            owner_id: user_ids.sample,
            resource_type: resource_type.camelize,
            resource_id: resource_type_ids.sample,
            comment: Faker::Books::Lovecraft.paragraph
          )
          counter += 1
          # Expected some uniq validation errors, if any will skip it and try again
        rescue StandardError
          next
        end
      end
    end

    count = 500
    puts "Creating post feedbacks ..."
    seeds_feedbacks(count, 'Post')

    count = 100
    puts "Creating user feedbacks ..."
    seeds_feedbacks(count, 'User')
  end
end
