require 'fileutils'

namespace :feedbacks do
  # rake feedbacks:generate_xml["$(pwd)/tmp/feedbacks_xml_files"]
  desc 'generate_xml'
  task :generate_xml, %i[folder_path] do |_t, args|
    folder_path = args[:folder_path].to_s

    DEFAULT_PER_LIMIT = 100

    base_sql = "
      SELECT
        feedbacks.id, feedbacks.owner_id,
        owner.username, owner.last_login,
        feedbacks.comment, feedbacks.created_at, feedbacks.resource_type as 'feedback_type',
        (case when posts.id is null then '' else CAST(ratings_total AS double) / posts.ratings_count end ) as ratings
      FROM feedbacks
      JOIN users owner on owner.id = feedbacks.owner_id
      LEFT JOIN posts on posts.id = feedbacks.resource_id and feedbacks.resource_type = 'Post'
      LEFT JOIN users on users.id = feedbacks.resource_id and feedbacks.resource_type = 'User'
    "

    # Create daily directory
    dirname = "#{folder_path}/#{Time.now.strftime("%Y_%m_%d")}"
    unless File.directory?(dirname)
      FileUtils.mkdir_p(dirname)
    end

    def write_file(full_filepath, data, root)
      File.open(full_filepath, 'w') { |file| file.write(data.as_json.to_xml(root: root)) }
    end

    files_path = []

    # Fetching and saving feedback in batches xml files
    batches = (Feedback.count / DEFAULT_PER_LIMIT).ceil
    (0..batches).each do |batch|
      offset = batch * DEFAULT_PER_LIMIT
      sql = base_sql + " LIMIT #{DEFAULT_PER_LIMIT} OFFSET #{offset};"
      data = ActiveRecord::Base.connection.execute(sql)

      # Saving the generated xml to a file with a given full_filepath
      full_filepath = "#{dirname}/feedbacks_#{batch+1}.xml"
      files_path << full_filepath
      write_file(full_filepath, data, :feedbacks)
    end

    # Writing root level xml file
    data = files_path.map do |e|
      {
        loc: e
      }
    end

    full_filepath = "#{dirname}/feedbacks.xml"
    write_file(full_filepath, data, :feedbackindex)
  end
end
