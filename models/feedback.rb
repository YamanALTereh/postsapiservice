class Feedback < ActiveRecord::Base
  belongs_to :resource, polymorphic: true
  belongs_to :owner, class_name: :User

  validates :comment, :owner_id, presence: true
  validates_uniqueness_of :resource_id, scope: [:resource_type, :owner_id], message: 'Already submitted Feedback for this resource'
end
