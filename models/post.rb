class Post < ActiveRecord::Base
  validates :user_id, :title, :content, :author_ip, presence: true, allow_blank: false
  belongs_to :user
  has_many :ratings
  has_many :feedbacks, as: :resource

  scope :recent, -> { order(created_at: :desc) }

  alias author user

  def average_rating
    avg = ratings_count.zero? ? 0.0 : (ratings_total.to_f / ratings_count.to_f)
    avg.round(2)
  end
end
