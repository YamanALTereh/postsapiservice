require 'counter_culture'

class Rating < ActiveRecord::Base
  MIN_RATING = 1
  MAX_RATING = 5

  validates_uniqueness_of :post_id, scope: :user_id, message: 'Already submitted rating for this post'
  validates :post_id, :value, presence: true, allow_blank: false
  validates_numericality_of :value,
    greater_than_or_equal_to: MIN_RATING,
    less_than_or_equal_to: MAX_RATING

  belongs_to :post

  counter_culture :post, column_name: :ratings_total, delta_column: :value
  counter_culture :post, column_name: :ratings_count
end
