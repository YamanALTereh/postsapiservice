class User < ActiveRecord::Base
  validates :username, :password_digest, presence: true
  has_secure_password

  has_many :posts
  has_many :feedbacks, as: :resource
  has_many :own_feedbacks, class_name: 'Feedback', foreign_key: :owner_id
end
