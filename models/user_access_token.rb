class UserAccessToken < ActiveRecord::Base
  validates :user_id, presence: true
  before_create :before_create
  after_create :update_last_login

  belongs_to :user

  scope :active, -> { where('revoked_at is null and expired_at > ?', Time.current) }

  def revoke!
    revoked_at = Time.current
    save!
  end

  private

  def before_create
    self.expired_at = 1.month.from_now
    self.token = SecureRandom.hex
  end

  def update_last_login
    user.update_column(
      :last_login, Time.current
    )
  end
end
