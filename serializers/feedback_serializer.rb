class FeedbackSerializer < ActiveModel::Serializer
  attributes :id, :resource_id, :resource_type, :comment, :owner_id, :created_at
end
