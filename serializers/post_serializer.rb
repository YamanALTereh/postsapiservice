class PostSerializer < ActiveModel::Serializer
  attributes :id, :title, :content, :average_rating, :user_id
end
